﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Vega.Controllers.Resources;
using Vega.Models;
using Vega.Core;
using System.Net;
using Microsoft.AspNetCore.Authorization;

namespace Vega.Controllers
{
    [Produces("application/json")]
    [Route("api/vehicles")]
    public class VehiclesController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IVehicleRepository _repository;

        public VehiclesController( IMapper mapper, IVehicleRepository repository, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _repository = repository;
            _unitOfWork = unitOfWork;
        }

        // GET: api/Vehicles
        [HttpGet]
        public async Task<QueryResult<VehicleResource>> GetVehicles(VehicleQueryResource filterResource)
        {
            var filter = _mapper.Map<VehicleQueryResource, VehicleQuery>(filterResource);
            var queryResult = await _repository.GetVehicles(filter);
                                  
            return _mapper.Map<QueryResult<Vehicle>, QueryResult<VehicleResource>>(queryResult);
        }

        // GET: api/Vehicles/5           
        [HttpGet("{id}")]
        public async Task<IActionResult> GetVehicle([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var vehicle = await _repository.GetVehicle(id);

            if (vehicle == null)
            {
                return NotFound();
            }

            var vr = _mapper.Map<Vehicle, VehicleResource>(vehicle);

            return Ok(vr);
        }

        // PUT: api/Vehicles/5
        [HttpPut("{id}")]
        [Authorize]
        public async Task<IActionResult> UpdateVehicle([FromRoute] int id, [FromBody] SaveVehicleResource vr)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var vehicle = await _repository.GetVehicle(id);

            VehicleResource result;

            try {
                vehicle = _mapper.Map<SaveVehicleResource, Vehicle>(vr, vehicle);
                vehicle.LastUpdate = DateTime.Now;
                await _unitOfWork.CompleteAsync();
                result = _mapper.Map<Vehicle, VehicleResource>(vehicle);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!_repository.Exists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(result);
        }

        // POST: api/Vehicles
        [HttpPost]
        [Authorize(Policies.RequireAdminRole)]
        public async Task<IActionResult> CreateVehicle([FromBody] SaveVehicleResource vr)
        {
            //checks data annotations from resource for input
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var vehicle = _mapper.Map<SaveVehicleResource, Vehicle>(vr);

            vehicle.LastUpdate = DateTime.Now;

            _repository.Add(vehicle);

            await _unitOfWork.CompleteAsync();

            vehicle = await _repository.GetVehicle(vehicle.Id);

            var result = _mapper.Map<Vehicle, VehicleResource>(vehicle);
            return Ok(result);
        }

        // DELETE: api/Vehicles/5
        [HttpDelete("{id}")]
        [Authorize]
        public async Task<IActionResult> DeleteVehicle([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var vehicle = await _repository.GetVehicle(id, includeRelated: false);

            if (vehicle == null)
            {
                return NotFound();
            }

            _repository.Remove(vehicle);
            await _unitOfWork.CompleteAsync();

            return Ok(vehicle);
        }

    }
}