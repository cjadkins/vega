import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { VehicleService } from '../../services/vehicle.service';
import { ToastyService } from 'ng2-toasty';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/Observable/forkJoin'
import { SaveVehicle, Vehicle, KeyValuePair } from '../../models/vehicle';
import { PaginationComponent } from '../pagination/pagination.component';
import { AuthService } from '../../services/auth.service';

@Component({
    selector: 'vehicle-list',
    templateUrl: './vehicle-list.component.html',
    styleUrls: ['./vehicle-list.component.css']
})

export class VehicleListComponent implements OnInit{
    private readonly PAGE_SIZE = 3;

    vehicles: Vehicle[];
    makes: KeyValuePair[];
    query: any = { pageSize: this.PAGE_SIZE}
    totalItems: number;
    columns = [
        { title: 'Id' },
        { title: 'Contact Name', key: 'contactName', isSortable: true},
        { title: 'Make', key: 'make', isSortable: true },
        { title: 'Model', key: 'model', isSortable: true },
        {  },
    ];

    constructor(private vehicleService: VehicleService,
                private authService: AuthService) { }

    ngOnInit() {
        this.vehicleService.getMakes()
            .subscribe(makes => this.makes = makes);

        this.populateVehicles();
    };

    private pluck(items: KeyValuePair[]) {
        var output = [];
        for (var item of items) {
            output.push(item.id);
        }
        return output;
    };  

    private populateVehicles() {
        this.vehicleService.getVehicles(this.query)
            .subscribe((result: any) => {
                this.vehicles = result.items;
                this.totalItems = result.totalItem;

            });
    }

    onFilterChange() {
        this.query.page = 1;
        this.populateVehicles();
    }

    resetFilter() {
        this.query = {
            page: 1,
            pageSize: this.PAGE_SIZE,
        };
        this.populateVehicles();
    }

    sortBy(columnName: string) {
        if (this.query.sortBy === columnName) {
            this.query.isSortAscending = !this.query.isSortAscending;
        } else {
            this.query.sortBy = columnName;
            this.query.isSortAscending = true;
        };
        this.populateVehicles();
    }
    onPageChange(page: any) {
        this.query.page = page;
        this.populateVehicles();
    }
}