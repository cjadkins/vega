import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { VehicleService } from '../../services/vehicle.service';
import { ToastyService } from 'ng2-toasty';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/Observable/forkJoin'
import { SaveVehicle, Vehicle, KeyValuePair } from '../../models/vehicle';

@Component({
    selector: 'vehicle-form',
    templateUrl: './vehicle-form.component.html',
    styleUrls: ['./vehicle-form.component.css']
})

export class VehicleFormComponent implements OnInit{
    features: any;
    makes: any;
    models: any;
    vehicle: SaveVehicle = {
        id: 0,
        makeId: 0,
        modelId: 0,
        isRegistered: false,
        features: [],
        contact: {
            name: '', 
            phone: '',
            email: ''
        },
    };

    constructor(
        private route: ActivatedRoute, // to read the route parameter
        private router:Router, // to route user to invalid page
        private vehicleService: VehicleService,
        private toastyService: ToastyService) {
            route.params.subscribe(p => {
                this.vehicle.id = +p['id']; //+ converts to number
        });
    }

    ngOnInit() {

        var sources = [
            this.vehicleService.getMakes(),
            this.vehicleService.getFeatures(),
        ];

        if (this.vehicle.id)
            sources.push(this.vehicleService.getVehicle(this.vehicle.id))

        Observable.forkJoin(sources)
            .subscribe(data => {
                this.makes = data[0];
                this.features = data[1];

                if (this.vehicle.id) {
                    this.setVehicle(data[2]);
                    this.populateModels();
                };
            }, err => {
                if (err.status == 404)
                    this.router.navigate(['/home']);
            });
    }

    private pluck(items: KeyValuePair[]) {
        var output = [];
        for (var item of items) {
            output.push(item.id);
        }
        return output;
    }

    private populateModels() {
        var selectedMake = this.makes.find((m: any) => m.id == this.vehicle.makeId);
        this.models = selectedMake.models;
    }

    private setVehicle(v: Vehicle) {
        this.vehicle.id = v.id;
        this.vehicle.makeId = v.make.id;
        this.vehicle.modelId = v.model.id;
        this.vehicle.isRegistered = v.isRegistered;
        this.vehicle.contact = v.contact;
        this.vehicle.features = this.pluck(v.features);
    }

    onMakeChange() {
        this.populateModels();
        delete this.vehicle.modelId;
    }

    onFeatureToggle(featureId: number, $event: any) {
        if ($event.target.checked) {
            this.vehicle.features.push(featureId);
        }
        else {
            var index = this.vehicle.features.indexOf(featureId);
            this.vehicle.features.splice(index, 1);
        }

    }

    submit() {
        if (this.vehicle.id) {
            this.vehicleService.updateVehicle(this.vehicle)
                .subscribe(x => {
                    this.toastyService.success({
                        title: 'Success',
                        msg: 'The vehicle was sucessfully updated.',
                        theme: 'bootstrap',
                        showClose: true,
                        timeout: 5000,
                    });
                    this.router.navigate(['/vehicles/view/' + this.vehicle.id]);
                });
        }
        else {
            this.vehicle.id = 0;
            this.vehicleService.createVehicle(this.vehicle)
                .subscribe((x:Vehicle) => {
                    this.toastyService.success({
                        title: 'Success',
                        msg: 'The vehicle was sucessfully created.',
                        theme: 'bootstrap',
                        showClose: true,
                        timeout: 5000,
                    
                    });
                    this.vehicle.id = x.id;

                    this.router.navigate(['/vehicles/view/' + this.vehicle.id]);
                });
        }
    }

    delete() {
        if (confirm("Are you sure?")) {
            this.vehicleService.deleteVehicle(this.vehicle.id)
                .subscribe(x => {
                    this.router.navigate(['/home']);
                });
        }
    }
}