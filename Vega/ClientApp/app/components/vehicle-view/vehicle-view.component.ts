import { Component, OnInit, ElementRef, ViewChild, NgZone } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { VehicleService } from '../../services/vehicle.service';
import { ToastyService } from 'ng2-toasty';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/Observable/forkJoin'
import { SaveVehicle, Vehicle, KeyValuePair } from '../../models/vehicle';
import { PaginationComponent } from '../pagination/pagination.component';
import { PhotoService } from '../../services/photo.service';
import { ProgressService, BrowserXhrWithProgress } from '../../services/progress.service';
import { BrowserXhr } from '@angular/http';
import { AuthService } from '../../services/auth.service';

@Component({
    selector: 'vehicle-view',
    templateUrl: './vehicle-view.component.html',
    styleUrls: ['./vehicle-view.component.css'],
    providers: [
        { provide: BrowserXhr, useClass: BrowserXhrWithProgress },
        ProgressService,
    ]
})

export class VehicleViewComponent implements OnInit{
    @ViewChild('fileInput') fileInput: ElementRef
    photos: any[];
    progress: any;

    vehicle: Vehicle = {
        id: 0,
        model: { id:0, name:"test" },
        make: { id:0, name:"test"},
        isRegistered: false,
        features: [],
        contact: {
            name: '',
            phone: '',
            email: ''
        },
        lastUpdate:''
    };

    constructor(
        private authService : AuthService,
        private zone: NgZone,
        private toastyService: ToastyService,
        private route: ActivatedRoute,
        private progressService: ProgressService,
        private vehicleService: VehicleService,
        private photoService: PhotoService,
        private router: Router)
    {
        route.params.subscribe(p => {
            this.vehicle.id = +p['id']; //+ converts to number
        });
    }

    ngOnInit() {
        this.photoService.getPhotos(this.vehicle.id)
            .subscribe((photos: any) => {
                console.log(photos);
                this.photos = photos;
            });

        this.vehicleService.getVehicle(this.vehicle.id)
            .subscribe(
            vehicle => this.vehicle = vehicle,
            err => {
                if (err.status == 404) {
                    this.router.navigate(['/vehicles']);
                    return;
                }
            });
    };

    delete() {
        if (confirm("Are you sure?")) {
            this.vehicleService.deleteVehicle(this.vehicle.id)
                .subscribe(x => {
                    this.router.navigate(['/vehicles']);
                });
        }
    }

    uploadPhoto() {

        this.progressService.startTrackingUpload()
            .subscribe(progress => {
                this.zone.run(() => {
                    this.progress = progress;
                });},
            err => null,
            () => { this.progress = null; }
        );
        var nativeElement: HTMLInputElement = this.fileInput.nativeElement;
        //! is the non-null assertion operator in ts.
        var file = nativeElement.files![0];
        nativeElement.value = '';

        this.photoService.upload(this.vehicle.id, file)
            .subscribe(photo => {
                this.photos.push(photo);
            },
            err => {
                this.toastyService.error({
                    title: 'Error',
                    msg: err.text(),
                    theme: 'bootstrap',
                    showClose: true,
                    timeout: 5000,
                });
            });
    }
}