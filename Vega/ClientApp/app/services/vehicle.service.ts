﻿import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Vehicle, SaveVehicle } from '../models/vehicle';
import { AuthHttp } from 'angular2-jwt';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class VehicleService {

    constructor(private http: Http,
                private httpNew: HttpClient,
                private authHttp: AuthHttp) { }

    private vehicleRoute: string = '/api/vehicles'

    private get authHeader(): string {
        return `Bearer ${localStorage.getItem('access_token')}`;
    }

    getMakes() {
        return this.http.get('/api/makes')
            .map(res => res.json());
    }

    getFeatures() {
        return this.http.get('/api/features')
            .map(res => res.json());
    }

    getVehicle(id: number) {
        return this.http.get(this.vehicleRoute + '/' + id)
            .map(res => res.json());
    }

    getVehicles(filters: any) {
        return this.http.get(this.vehicleRoute + '?' + this.toQueryString(filters))
            .map(res => res.json());
    }

    toQueryString(obj: any) {
        var parts = [];
        for (var property in obj) {
            var value = obj[property];
            if (value != null && value != undefined)
                parts.push(encodeURIComponent(property) + '=' + encodeURIComponent(value))
        }

        return parts.join('&')
    }

    createVehicle(vehicle: any) {
        return this.httpNew.post(this.vehicleRoute, vehicle, { headers: new HttpHeaders().set('Authorization', this.authHeader) })
            //.map(res => res.json());
    }

    updateVehicle(vehicle: any) {
        return this.httpNew.put(this.vehicleRoute + '/' + vehicle.id, vehicle, { headers: new HttpHeaders().set('Authorization', this.authHeader) })
            //.map(res => res.json());
    }

    deleteVehicle(id: number) {
        return this.httpNew.delete(this.vehicleRoute + '/' + id, { headers: new HttpHeaders().set('Authorization', this.authHeader) })
            //.map(res => res.json());
    }
}