﻿import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import 'rxjs/add/operator/filter';
import * as auth0 from 'auth0-js';
import { JwtHelper } from 'angular2-jwt';

@Injectable()
export class AuthService {

    profile: any = {};
    private roles: string[] = [];

    auth0 = new auth0.WebAuth({
        clientID: 'Bqu3vRj8R9gIhWQqfPFa4m3E86bZdIy3',
        domain: 'vegaprojca.auth0.com',
        responseType: 'token id_token',
        audience: 'https://api.vega.com',
        redirectUri: 'http://localhost:55848',
        scope: 'openid email profile'
    });

    constructor(public router: Router) {
        this.readProfileFromLocalStorage();
        this.readRolesFromLocalStorage();
    }

    public login(): void {
        this.auth0.authorize();
    }

    public handleAuthentication(): void {
        this.auth0.parseHash((err: any, authResult: any) => {
            if (authResult && authResult.accessToken && authResult.idToken) {
                window.location.hash = '';
                this.setSession(authResult);
                this.readRolesFromLocalStorage();
                this.router.navigate(['/home']);
            } else if (err) {
                this.router.navigate(['/home']);
                console.log(err);
            }
        });
    }

    private readProfileFromLocalStorage(): void {
        this.profile = JSON.parse(localStorage.getItem('profile') || '{}');
    }

    private readRolesFromLocalStorage(): void {
        var token = localStorage.getItem('access_token');
        if (token) {
            var jwtHelper = new JwtHelper();
            var decodedToken = jwtHelper.decodeToken(token);
            this.roles = decodedToken['https://vega.com/roles'] || [];
        }
    }

    private setUserProfile(authResult: any): void {
        this.auth0.client.userInfo(authResult.accessToken, function(this: any, err: any, profile: any) {
            if (err)
                throw err;

            localStorage.setItem('profile', JSON.stringify(profile) || '{}');
        });
    }

    private setSession(authResult: any): void {
        // Set the time that the Access Token will expire at
        const expiresAt = JSON.stringify((authResult.expiresIn * 1000) + new Date().getTime());
        localStorage.setItem('access_token', authResult.accessToken);
        localStorage.setItem('id_token', authResult.idToken);
        localStorage.setItem('expires_at', expiresAt);
        this.setUserProfile(authResult);
    }

    public logout(): void {
        // Remove tokens and expiry time from localStorage
        localStorage.removeItem('access_token');
        localStorage.removeItem('id_token');
        localStorage.removeItem('expires_at');
        localStorage.removeItem('profile');
        this.profile = {};
        this.roles = [];
        // Go back to the home route
        this.router.navigate(['/']);
    }

    public isAuthenticated(): boolean {
        // Check whether the current time is past the
        // Access Token's expiry time
        const expiresAt = JSON.parse(localStorage.getItem('expires_at') || '0');
        return new Date().getTime() < expiresAt;
    }

    public isInRole(roleName: string): boolean {
        return this.roles.indexOf(roleName) > -1;
    }

}