﻿import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { BrowserXhr } from '@angular/http';

@Injectable()
export class ProgressService {
    private uploadProgress: Subject<any>;
    private downloadProgress: Subject<any>;

    startTrackingUpload() {
        this.uploadProgress = new Subject();
        return this.uploadProgress;
    };

    startTrackingDownload() {
        this.downloadProgress = new Subject();
        return this.downloadProgress;
    }

    notifyUp(progress: any) {
        if (this.uploadProgress)
            this.uploadProgress.next(progress);
    }

    notifyDown(progress: any) {
        if (this.downloadProgress)
            this.downloadProgress.next(progress);
    }

    endTrackingUpload() {
        if (this.uploadProgress)
            this.uploadProgress.complete();
    }

    endTrackingDownload() {
        if (this.downloadProgress)
            this.downloadProgress.complete();
    }

}

@Injectable()
export class BrowserXhrWithProgress extends BrowserXhr {

    constructor(private service: ProgressService) { super(); }

    build(): XMLHttpRequest {
        var xhr: XMLHttpRequest = super.build();

        xhr.onloadstart = () => {
            this.service.startTrackingDownload();
        }

        xhr.onprogress = (event) => {
            this.service.notifyDown(this.createProgress(event));
        };

        xhr.onloadend = () => {
            this.service.endTrackingDownload();
        }

        xhr.upload.onloadstart = () => {
            this.service.startTrackingUpload();
        };

        xhr.upload.onprogress = (event) => {
            this.service.notifyUp(this.createProgress(event));
        };

        xhr.upload.onloadend = () => {
            this.service.endTrackingUpload();
        }

        return xhr;
    }

    private createProgress(event: any) {
        return {
            total: event.total,
            percentage: Math.round(event.loaded / event.total * 100)
        }
    }
}