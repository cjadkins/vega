﻿using Microsoft.EntityFrameworkCore;
using Vega.Models;

namespace Vega.Core
{
    public class VegaDbContext : DbContext
    {
        public DbSet<Vehicle> Vehicles { get; set; }
        public DbSet<Make> Makes { get; set; }
        public DbSet<Model> Models { get; set; }
        public DbSet<Feature> Features { get; set; }
        public DbSet<Photo> Photos { get; set; }

        public VegaDbContext(DbContextOptions<VegaDbContext> options)
            : base(options)
        {
        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<Vehicle>().OwnsOne(p => p.Contact, cb =>
            //{
            //    cb.OwnsOne(c => c.Name);
            //    cb.OwnsOne(c => c.Phone);
            //    cb.OwnsOne(c => c.Email);
            //});

            //these composite keys can be done with data annotations
            modelBuilder.Entity<VehicleFeature>().HasKey(vf =>
               new { vf.VehicleID, vf.FeatureID });
        }
    }
}
