﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Vega.Models
{

    [Table("Features")]
    public class Feature
    {
        public int Id { get; set; }

        public string Name { get; set; }

        //public ICollection<Model> ModelIds { get; set; }
    }
}
