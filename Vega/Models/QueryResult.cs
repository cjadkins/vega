﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Vega.Models
{
    public class QueryResult<T>
    {
        public int TotalItem { get; set; }
        public IEnumerable<T> Items { get; set; }
    }
}
